import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import {faInstagram, faTiktok, faYoutube, faFacebook, faTwitch, faTwitter} from '@fortawesome/free-brands-svg-icons';
import { filter } from 'rxjs/operators';

declare var gtag:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frec420';

  faInstagram = faInstagram;
  faTiktok = faTiktok;
  faYoutube = faYoutube;
  faFacebook = faFacebook;
  faTwitch = faTwitch;
  faTwitter = faTwitter;

  constructor(
    private titleService: Title,
    private router: Router
  ){
    this.titleService.setTitle(this.title)

    const navEndEvent$ = this.router.events
    .pipe(
      filter(event => event instanceof NavigationEnd)
    );

    navEndEvent$.subscribe((event)=> {
      gtag('config', 'G-WQ8XPHFNXS', {
        page_path: event
      });
    });

  }
}
